from django.urls import path, re_path, include
from basic_app.views import relative, other

app_name = 'basic_app'

urlpatterns = [
    path('relative/', relative, name='relative'),
    path('other/', other, name='other'),
]
